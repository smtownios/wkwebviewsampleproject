//
//  ViewController.m
//  WkWebviewSampleProject
//
//  Created by JY on 2018. 11. 29..
//  Copyright © 2018년 JY. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>

@interface ViewController () <WKNavigationDelegate, WKScriptMessageHandler>

@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) NSURLRequest *request;

// Toolbar buttons
@property (strong, nonatomic) UIBarButtonItem *backButton;
@property (strong, nonatomic) UIBarButtonItem *forwardButton;
@property (strong, nonatomic) UIBarButtonItem *refreshButton;
@property (strong, nonatomic) UIBarButtonItem *homeButton;

@end

@implementation ViewController

- (void)loadView {
    //A collection of properties used to initialize a web view.
    WKWebViewConfiguration *webConfiguration = [[WKWebViewConfiguration alloc] init];
    
    //A WKUserContentController object provides a way for JavaScript to post messages and inject user scripts to a web view.
    WKUserContentController *contentController = [[WKUserContentController alloc] init];
    [contentController addScriptMessageHandler:self name:@"handlerForApp"];
    webConfiguration.userContentController = contentController;
    
    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:webConfiguration];
    self.webView.navigationDelegate = self;
    
    self.view = self.webView; 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setToolbar];
    
    NSString *urlString = @"http://nowapibeta.smtown.com:8082/wv/news/detail/388780?system=0&userId=0&lc=en";
    NSURL *url = [NSURL URLWithString:urlString];
    self.request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:self.request];
}

- (void)setToolbar {
    [self.navigationController setToolbarHidden:false];
    
    [self makeButtonWithImage:back];
    [self makeButtonWithImage:forward];
    [self makeButtonWithImage:home];
    self.refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    NSArray *barButtonItems = [NSArray arrayWithObjects:self.backButton, space, self.forwardButton, space, self.refreshButton, space, self.homeButton, nil];
    self.toolbarItems = barButtonItems;
}

enum buttonType { back, forward, refresh, home };
- (void)makeButtonWithImage:(enum buttonType)type {
    UIImage *image;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    switch (type) {
        case back:
            image = [UIImage imageNamed:@"BackButton.png"];
            [button addTarget:self action:@selector(goBack)forControlEvents:UIControlEventTouchUpInside];
            self.backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
            break;
        case forward:
            image = [UIImage imageNamed:@"ForwardButton.png"];
            [button addTarget:self action:@selector(goForward)forControlEvents:UIControlEventTouchUpInside];
            self.forwardButton = [[UIBarButtonItem alloc] initWithCustomView:button];
            break;
        case home:
            image = [UIImage imageNamed:@"HomeButton.png"];
            [button addTarget:self action:@selector(goHome)forControlEvents:UIControlEventTouchUpInside];
            self.homeButton = [[UIBarButtonItem alloc] initWithCustomView:button];
            break;
        default:
            break;
    }
    [button setImage:image forState:UIControlStateNormal];
}

- (void)goBack {
    [self.webView goBack];
}

- (void)goForward {
    [self.webView goForward];
}

- (void)refresh {
    [self.webView reload];
}

- (void)goHome {
    [self.webView loadRequest:self.request];
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    self.title = @"Loading...";
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
//    title 읽어오기
//    self.title = self.webView.title;
    [self.webView evaluateJavaScript:@"document.title" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        if(result){
            self.title = result;
        }
    }];
    
    if([self.webView canGoBack]){
        [self.backButton setEnabled:true];
    } else {
        [self.backButton setEnabled:false];
    }
    if([self.webView canGoForward]){
        [self.forwardButton setEnabled:true];
    } else {
        [self.forwardButton setEnabled:false];
    }
}

//A class conforming to the WKScriptMessageHandler protocol provides a method for receiving messages from JavaScript running in a webpage.
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
//    NSLog(@"%@", message.name);
//    NSLog(@"%@", message.body);
    NSString *value = [self getValueFromMessage:message];
    [self makeAlert:@"action" message:value];
}

- (NSString *)getValueFromMessage:(WKScriptMessage *)message {
    if(message){
        NSDictionary *messageBody = [NSJSONSerialization JSONObjectWithData:[message.body dataUsingEncoding:NSUTF8StringEncoding] options:0 error: nil];
        if(messageBody){
            return [messageBody[@"action"] stringValue];
        } else {
            NSLog(@"messageBody is nil");
        }
    } else {
        NSLog(@"message is nil");
    }
    return @"Error";
}

- (void)makeAlert:(NSString *)title message:(NSString *)messageString {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:messageString preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
